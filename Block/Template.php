<?php

namespace Swiss\Footer\Block;

use Magento\Store\Model\ScopeInterface;

class Template extends \Magento\Framework\View\Element\Template
{
    private $_store;
    private $_information;
    protected $_scopeConfig;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\Information $information,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->_store = $context->getStoreManager()->getStore();
        $this->_information = $information->getStoreInformationObject($this->_store);
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function getStoreName()
    {
        return $this->_store->getName();
    }

    public function getStoreCode()
    {
        return $this->_store->getCode();
    }

    public function getStoreEmail()
    {
        return $this->_scopeConfig->getValue('trans_email/ident_support/email', ScopeInterface::SCOPE_STORE);
    }

    public function getStorePhone()
    {
        return $this->_information->getPhone();
    }

    public function getStoreAddress()
    {
        return $this->_information['street_line1'];
    }

    public function getStorePostcode()
    {
        return $this->_information->getPostcode();
    }

    public function getStoreCity()
    {
        return $this->_information->getCity();
    }

    public function getStoreCountry()
    {
        return $this->_information->getCountry();
    }

}

?>